using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PizzariaLuiz.Dominio.Entidade;
using PizzariaLuiz.Dominio.Repositorio.Classe;
using PizzariaLuiz.Dominio.Repositorio.Interface;
using System.Linq;

namespace PizzariaLuiz.Controllers
{
    [Route("api/[controller]")]
    [ApiController] 
    public class ListaPedidosController : ControllerBase
    {
        IRepositorio<ListaPedidos> repositorio;

        public ListaPedidosController(IConfiguration config)
        {
            var cp = new ProvedorConexao(config);
            repositorio = new RepositorioListaPedidos(cp);
        }
        /// <summary>  
        ///  Retorna todos os pedidos cadastrados para montagem de relatório
        /// </summary> 
        [HttpGet]
        public ActionResult<IEnumerable<ListaPedidos>> Get()
        {
            var result = repositorio.ObterTodos().ToList();
            return result;
        }        
    }
}