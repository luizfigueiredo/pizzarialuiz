using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PizzariaLuiz.Dominio.Entidade;
using PizzariaLuiz.Dominio.Repositorio.Classe;
using PizzariaLuiz.Dominio.Repositorio.Interface;
using System.Linq;

namespace PizzariaLuiz.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
       IRepositorio<Produto> repositorio;

        public ProdutoController(IConfiguration config)
        {
            var cp = new ProvedorConexao(config);
            repositorio = new RepositorioProduto(cp);
        }
        /// <summary>  
        ///  Retorna todos os produtos cadastrados 
        /// </summary> 
        [HttpGet]
        public IEnumerable<Produto> Get()
        {
            var result = repositorio.ObterTodos().ToList();
            return result;
        }        
        /// <summary>  
        ///  Retorna os dados de um produto pelo seu ID 
        /// </summary>        
        [HttpGet("{id}")]        
        public Produto Get(int id)
        {
            var result = repositorio.Obter(id); 
            return result;             
        } 
        /// <summary>  
        /// Cadastra um produto
        /// </summary> 
        [HttpPost]
        public ActionResult Post([FromBody] Produto produto)
        {
            if (CamposOk(produto))
            { 
               repositorio.Inserir(produto);
               return new OkObjectResult("Produto cadastrado com sucesso!");
            }            
            return new BadRequestObjectResult("Operação não realizada. Campos obrigatórios não informados.");
        }       
        /// <summary>  
        ///  Altera os dados de um produto pelo seu ID 
        /// </summary>              
        [HttpPut] 
        public ActionResult Put([FromBody] Produto produto)
        {
            if (CamposOk(produto))
            { 
              repositorio.Atualizar(produto);
              return new OkObjectResult("Produto alterado com sucesso!");
            }            
            return new BadRequestObjectResult("Operação não realizada. Campos obrigatórios não informados.");            
        }        
        /// <summary>  
        ///  Exclui um produto pelo seu ID 
        /// </summary>   
         [HttpDelete]
        public ActionResult Delete(Produto produto)
        {
            if (AchouRegistro(produto.Id))
            {
                repositorio.Deletar(produto);
                return new OkObjectResult("Produto excluído com sucesso!");
            }    
            return new BadRequestObjectResult("Operação não realizada. Produto não cadastrado.");  
        }
        /// <summary>  
        ///  Verifica o preenchimento dos campos obrigatórios do produto 
        /// </summary> 
        private bool CamposOk(Produto produto)
        {
            return (produto.Nome != null && produto.Preco != 0);                    
        }
        /// <summary>  
        ///  Verifica se o produto está cadastrado
        /// </summary> 
        public bool AchouRegistro(int id)  
        {                     
            return (repositorio.Obter(id) != null);            
        }       
    }
}