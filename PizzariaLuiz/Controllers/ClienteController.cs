using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PizzariaLuiz.Dominio.Entidade;
using PizzariaLuiz.Dominio.Repositorio.Classe;
using PizzariaLuiz.Dominio.Repositorio.Interface;
using System.Linq;

namespace PizzariaLuiz.Controllers
{
    [Route("api/[controller]")]    
    [ApiController]
    public class ClienteController : ControllerBase
    {
        IRepositorio<Cliente> repositorio;
       
        public ClienteController(IConfiguration config)
        {
            var cp = new ProvedorConexao(config);
            repositorio = new RepositorioCliente(cp);
        }
        /// <summary>  
        ///  Retorna todos os clientes cadastrados 
        /// </summary>         
        [HttpGet]        
        public IEnumerable<Cliente> Get()
        {
            var result = repositorio.ObterTodos().ToList();
            return result;
        }
        /// <summary>  
        ///  Retorna os dados de um cliente pelo seu ID 
        /// </summary>        
        [HttpGet("{id}")]        
        public Cliente Get(int id)
        {
            var result = repositorio.Obter(id); 
            return result;             
        }
        /// <summary>  
        /// Cadastra um cliente  
        /// </summary> 
        [HttpPost]
        public ActionResult Post([FromBody] Cliente cliente)
        {
            if (CamposOk(cliente))
            { 
               repositorio.Inserir(cliente);
               return new OkObjectResult("Cliente cadastrado com sucesso!");
            }            
            return new BadRequestObjectResult("Operação não realizada. Campos obrigatórios não informados.");
        }       
        /// <summary>  
        ///  Altera os dados de um cliente pelo seu ID 
        /// </summary>              
        [HttpPut] 
        public ActionResult Put([FromBody] Cliente cliente)
        {
            if (CamposOk(cliente))
            { 
              repositorio.Atualizar(cliente);
              return new OkObjectResult("Cliente alterado com sucesso!");
            }            
            return new BadRequestObjectResult("Operação não realizada. Campos obrigatórios não informados.");            
        }        
        /// <summary>  
        ///  Exclui um cliente pelo seu ID 
        /// </summary>   
         [HttpDelete]
        public ActionResult Delete(Cliente cliente)
        {
            if (AchouRegistro(cliente.Id))
            {
                repositorio.Deletar(cliente);
                return new OkObjectResult("Cliente excluído com sucesso!");
            }    
            return new BadRequestObjectResult("Operação não realizada. Cliente não cadastrado.");  
        }
        /// <summary>  
        ///  Verifica o preenchimento dos campos obrigatórios do cliente 
        /// </summary> 
        private bool CamposOk(Cliente cliente)
        {
            return (cliente.Nome != null && cliente.Telefone != null && cliente.Endereco != null);                    
        }
        /// <summary>  
        ///  Verifica se o cliente está cadastrado
        /// </summary> 
        public bool AchouRegistro(int id)  
        {                     
            return (repositorio.Obter(id) != null);            
        }
    }
}