using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PizzariaLuiz.Dominio.Entidade;
using PizzariaLuiz.Dominio.Repositorio.Classe;
using PizzariaLuiz.Dominio.Repositorio.Interface;
using System.Linq;

namespace PizzariaLuiz.Controllers
{
    [Route("api/[controller]")]
    [ApiController] 
    public class PedidoController : ControllerBase
    {
        IRepositorio<Pedido> repositorio;

        public PedidoController(IConfiguration config)
        {
            var cp = new ProvedorConexao(config);
            repositorio = new RepositorioPedido(cp);
        }
        /// <summary>  
        ///  Retorna todos os pedidos cadastrados 
        /// </summary> 
        [HttpGet]
        public ActionResult<IEnumerable<Pedido>> Get()
        {
            var result = repositorio.ObterTodos().ToList();
            return result;
        }
        /// <summary>  
        ///  Retorna os dados de um pedido pelo seu ID 
        /// </summary>  
        [HttpGet("{id}")]        
        public Pedido Get(int id)
        {
           var result = repositorio.Obter(id);
           return result;
        }  
        /// <summary>  
        /// Cadastra um pedido
        /// </summary> 
        [HttpPost]
        public ActionResult Post([FromBody] Pedido pedido)
        {
            if (CamposOk(pedido))
            { 
               repositorio.Inserir(pedido);
               return new OkObjectResult("Pedido cadastrado com sucesso!");
            }            
            return new BadRequestObjectResult("Operação não realizada. Campos obrigatórios não informados.");
        }       
        /// <summary>  
        ///  Altera os dados de um pedido pelo seu ID 
        /// </summary>              
        [HttpPut] 
        public ActionResult Put([FromBody] Pedido pedido)
        {
            if (CamposOk(pedido))
            { 
              repositorio.Atualizar(pedido);
              return new OkObjectResult("Pedido alterado com sucesso!");
            }            
            return new BadRequestObjectResult("Operação não realizada. Campos obrigatórios não informados.");            
        }        
        /// <summary>  
        ///  Exclui um pedido pelo seu ID 
        /// </summary>   
         [HttpDelete]
        public ActionResult Delete(Pedido pedido)
        {
            if (AchouRegistro(pedido.Id))
            {
                repositorio.Deletar(pedido);
                return new OkObjectResult("Pedido excluído com sucesso!");
            }    
            return new BadRequestObjectResult("Operação não realizada. Pedido não cadastrado.");  
        }
        /// <summary>  
        ///  Verifica o preenchimento dos campos obrigatórios do pedido 
        /// </summary> 
        private bool CamposOk(Pedido pedido)
        {
            return (pedido.DataHora != null && pedido.IdCliente != 0 && pedido.Total != 0);                    
        }
        /// <summary>  
        ///  Verifica se o pedido está cadastrado
        /// </summary> 
        public bool AchouRegistro(int id)  
        {                     
            return (repositorio.Obter(id) != null);            
        }       
    }
}