using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PizzariaLuiz.Dominio.Entidade;
using PizzariaLuiz.Dominio.Repositorio.Classe;
using PizzariaLuiz.Dominio.Repositorio.Interface;
using System.Linq;

namespace PizzariaLuiz.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidoItemController : ControllerBase
    {
        IRepositorio<PedidoItem> repositorio;

        public PedidoItemController(IConfiguration config)
        {
            var cp = new ProvedorConexao(config);
            repositorio = new RepositorioPedidoItem(cp);
        }
        /// <summary>  
        ///  Retorna todos os itens pedidos cadastrados 
        /// </summary>
        [HttpGet]
        public ActionResult<IEnumerable<PedidoItem>> Get()
        {
            var result = repositorio.ObterTodos().ToList();
            return result;
        }
        /// <summary>  
        ///  Retorna os dados de um item pedido pelo seu ID 
        /// </summary>  
        [HttpGet("{id}")]        
        public PedidoItem Get(int id)
        {
           var result = repositorio.Obter(id);
           return result;
        } 
        /// <summary>  
        /// Cadastra um item do pedido
        /// </summary> 
        [HttpPost]
        public ActionResult Post([FromBody] PedidoItem pedidoItem)
        {
            if (CamposOk(pedidoItem))
            { 
               repositorio.Inserir(pedidoItem);
               return new OkObjectResult("Item do pedido cadastrado com sucesso!");
            }            
            return new BadRequestObjectResult("Operação não realizada. Campos obrigatórios não informados.");
        }       
        /// <summary>  
        ///  Altera os dados de um item do pedido pelo seu ID 
        /// </summary>              
        [HttpPut] 
        public ActionResult Put([FromBody] PedidoItem pedidoItem)
        {
            if (CamposOk(pedidoItem))
            { 
              repositorio.Atualizar(pedidoItem);
              return new OkObjectResult("Item do pedido alterado com sucesso!");
            }            
            return new BadRequestObjectResult("Operação não realizada. Campos obrigatórios não informados.");            
        }        
        /// <summary>  
        ///  Exclui um item do pedido pelo seu ID 
        /// </summary>   
         [HttpDelete]
        public ActionResult Delete(PedidoItem pedidoItem)
        {
            if (AchouRegistro(pedidoItem.Id))
            {
                repositorio.Deletar(pedidoItem);
                return new OkObjectResult("Item do pedido excluído com sucesso!");
            }    
            return new BadRequestObjectResult("Operação não realizada. Item do pedido não cadastrado.");  
        }
        /// <summary>  
        ///  Verifica o preenchimento dos campos obrigatórios do item do pedido 
        /// </summary> 
        private bool CamposOk(PedidoItem pedidoItem)
        {
            return (pedidoItem.IdPedido != 0 && pedidoItem.IdProduto != 0 && pedidoItem.Quantidade != 0);                    
        }
        /// <summary>  
        ///  Verifica se o item do pedido está cadastrado
        /// </summary> 
        public bool AchouRegistro(int id)  
        {                     
            return (repositorio.Obter(id) != null);            
        }   
    }
}