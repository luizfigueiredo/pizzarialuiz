using System;

namespace PizzariaLuiz.Dominio.Entidade
{
    /// <summary>
    /// Entidade Lista de pedidos
    /// </summary>
    public class ListaPedidos
    {
     
        public int NumeroPedido { get; set;}
        public DateTime DataPedido { get;set;}
        public string NomeCliente { get; set;}
        public decimal TotalPedido { get; set;} 
        public string DescricaoItem  { get; set;} 
        public decimal PrecoUnitario { get; set;} 
        public int Quantidade { get; set;}
        public decimal TotalItem { get; set;} 

    }
}