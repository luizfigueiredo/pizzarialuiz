namespace PizzariaLuiz.Dominio.Entidade
{
    /// <summary>
    /// Entidade Produto
    /// </summary>
    public class Produto
    {
        public int Id { get; set;}
        public string Nome { get; set;}
        public decimal Preco { get; set;}
    }
}