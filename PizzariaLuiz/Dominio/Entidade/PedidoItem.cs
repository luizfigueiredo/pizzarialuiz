namespace PizzariaLuiz.Dominio.Entidade
{
    /// <summary>
    /// Entidade PedidoItem 
    /// </summary> 
    public class PedidoItem
    {
        public int Id { get; set;}
        public int IdPedido { get; set;}
        public int IdProduto { get; set;}
        public int Quantidade { get; set;}
    }
}