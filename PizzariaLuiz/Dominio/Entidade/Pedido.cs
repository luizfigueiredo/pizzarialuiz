using System;
namespace PizzariaLuiz.Dominio.Entidade
{
    /// <summary>
    /// Entidade Pedido 
    /// </summary>
    public class Pedido
    {
        public int Id { get; set;}
        public DateTime DataHora { get;set;}
        public int IdCliente { get;set;}
        public decimal Total { get;set;}
    }
}