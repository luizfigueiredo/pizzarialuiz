using System.Data;
namespace PizzariaLuiz.Dominio.Repositorio.Interface
{
    public interface IConnection
    {
        IDbConnection CriarConexao();
    }
}