using System.Collections.Generic;

namespace PizzariaLuiz.Dominio.Repositorio.Interface
{
    public interface IRepositorio<T> where T : new()
    {
        IEnumerable<T> ObterTodos();
        T Obter(int id);
        void Inserir(T obj);
        void Atualizar(T obj);
        void Deletar(T obj);          
    }
}