using System.Collections.Generic;
using PizzariaLuiz.Dominio.Entidade;
using PizzariaLuiz.Dominio.Repositorio.Interface;
using Dapper;

namespace PizzariaLuiz.Dominio.Repositorio.Classe
{
    public class RepositorioProduto : IRepositorio<Produto>
    {
        IConnection con;

        public RepositorioProduto(IConnection conexao)
        {
            this.con = conexao;
        }
        /// <summary>  
        ///  Inseri o produto na base de dados
        /// </summary>
        public void Inserir(Produto obj)
        {
            using(var conexao = con.CriarConexao())
            {
                conexao.Query($@"INSERT INTO produto VALUES
                                (@Id, @Nome, @Preco)", new
                {
                    Id = obj.Id,
                    Nome = obj.Nome,
                    Preco = obj.Preco
                });
            }            
        }
        /// <summary>  
        ///  Atualiza o produto na base de dados
        /// </summary>
        public void Atualizar(Produto obj)
        {
            using(var conexao = con.CriarConexao())
            {
                conexao.Query($@"UPDATE produto SET 
                            nome = @Nome, 
                            preco = @Preco                             
                            where id = (@id)", new
                {
                    Id = obj.Id,
                    Nome = obj.Nome,
                    Preco = obj.Preco
                });
            }           
        } 
        /// <summary>       
        ///  Exclui o produto na base de dados
        /// </summary>
        public void Deletar(Produto obj)
        {
            using(var conexao = con.CriarConexao())
            {
                conexao.Query($@"DELETE FROM produto WHERE id = @Id", new
                {
                    Id = obj.Id
                });
            }        
        }        
        /// <summary>  
        ///  Obtem o produto a partir do seu ID na base de dados
        /// </summary>
        public Produto Obter(int id)
        {
            var sql = "SELECT id, nome, preco FROM produto WHERE id = @id";
            using(var conexao = con.CriarConexao())
            {                
                return conexao.QueryFirstOrDefault<Produto>(sql, new { Id = id });
            }           
        }
        /// <summary>  
        /// Obtem todos os produtos na base de dados
        /// </summary> 
        public IEnumerable<Produto> ObterTodos()
        {
            var sql = "SELECT id, nome, preco FROM produto";

            using(var conexao = con.CriarConexao())
            {
                return conexao.Query<Produto>(sql);
            }
        }   
    }
}