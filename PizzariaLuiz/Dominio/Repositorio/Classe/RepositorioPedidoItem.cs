using System.Collections.Generic;
using PizzariaLuiz.Dominio.Entidade;
using PizzariaLuiz.Dominio.Repositorio.Interface;
using Dapper;

namespace PizzariaLuiz.Dominio.Repositorio.Classe
{
    public class RepositorioPedidoItem : IRepositorio<PedidoItem>
    {
        IConnection con;

        public RepositorioPedidoItem(IConnection conexao)
        {
            this.con = conexao;
        }

        /// <summary>  
        ///  Inseri o item do pedido na base de dados
        /// </summary>
        public void Inserir(PedidoItem obj)
        {
            using(var conexao = con.CriarConexao())
            {
                conexao.Query($@"INSERT INTO pedidoitem VALUES
                                (@Id, @IdPedido, @IdProduto, @Qtde)", new
                {
                    Id = obj.Id,
                    IdPedido = obj.IdPedido,
                    IdProduto = obj.IdProduto,
                    Qtde = obj.Quantidade
                });
            }            
        }
        /// <summary>  
        ///  Atualiza o item do pedido na base de dados
        /// </summary>
        public void Atualizar(PedidoItem obj)
        {
            using(var conexao = con.CriarConexao())
            {
                conexao.Query($@"UPDATE pedidoitem SET
                            idpedido = @IdPedido,
                            idproduto = @IdProduto, 
                            qtde = @Qtde                             
                            where id = (@id)", new
                {
                    Id = obj.Id,
                    IdPedido = obj.IdPedido,
                    IdProduto = obj.IdProduto,
                    Qtde = obj.Quantidade
                });
            }           
        } 
        /// <summary>       
        ///  Exclui o item do pedido na base de dados
        /// </summary>
        public void Deletar(PedidoItem obj)
        {
            using(var conexao = con.CriarConexao())
            {
                conexao.Query($@"DELETE FROM pedidoitem WHERE id = @Id", new
                {
                    Id = obj.Id
                });
            }        
        }
        /// <summary>  
        ///  Obtem o item do pedido a partir do seu ID na base de dados
        /// </summary>
        public PedidoItem Obter(int id)
        {            
            var sql = "SELECT id, idPedido, idProduto, qtde FROM pedidoItem WHERE id = @id";

            using(var conexao = con.CriarConexao())
            {                
                return conexao.QueryFirstOrDefault<PedidoItem>(sql, new { Id = id });
            }          
        }
        /// <summary>  
        /// Obtem todos os itens do pedido na base de dados
        /// </summary> 
        public IEnumerable<PedidoItem> ObterTodos()
        {
            var sql = "SELECT id, idPedido, idProduto, qtde FROM pedidoItem";

            using(var conexao = con.CriarConexao())
            {
                return conexao.Query<PedidoItem>(sql);
            }
        }     
    }
}