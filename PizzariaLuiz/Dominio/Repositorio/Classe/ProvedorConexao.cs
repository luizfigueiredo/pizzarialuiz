using System.Data;
using Microsoft.Extensions.Configuration;
using Npgsql;
using PizzariaLuiz.Dominio.Repositorio.Interface;

namespace PizzariaLuiz.Dominio.Repositorio.Classe
{
    public class ProvedorConexao : IConnection
    {
        IConfiguration configurador;
        private string ConnectionString;

        public ProvedorConexao() // construtor padrao - não sei se usarei
        {
        }

        public ProvedorConexao(IConfiguration config)
        {
            configurador = config;
        }

        public IDbConnection CriarConexao()
        {
            ConnectionString = configurador["ConnectionStrings:Database"];
            return new NpgsqlConnection(ConnectionString);
        }    
    }
}