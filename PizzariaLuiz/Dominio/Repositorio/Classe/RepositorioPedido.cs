using System.Collections.Generic;
using PizzariaLuiz.Dominio.Entidade;
using PizzariaLuiz.Dominio.Repositorio.Interface;
using Dapper;

namespace PizzariaLuiz.Dominio.Repositorio.Classe
{
    public class RepositorioPedido : IRepositorio<Pedido>
    {
      IConnection con;

        public RepositorioPedido(IConnection conexao)
        {
            this.con = conexao;
        }
        /// <summary>  
        ///  Inseri o pedido na base de dados
        /// </summary>
        public void Inserir(Pedido obj)
        {
            using(var conexao = con.CriarConexao())
            {
                conexao.Query($@"INSERT INTO pedido VALUES
                                (@Id, @DataHora, @IdCliente, @Total)", new
                {
                    Id = obj.Id,
                    DataHora = obj.DataHora,
                    IdCliente = obj.IdCliente,
                    Total = obj.Total
                });
            }            
        }
        /// <summary>  
        ///  Atualiza o pedido na base de dados
        /// </summary>
        public void Atualizar(Pedido obj)
        {
            using(var conexao = con.CriarConexao())
            {
                conexao.Query($@"UPDATE pedido SET
                            datahora = @DataHora,
                            idcliente = @IdCliente, 
                            total = @Total                             
                            where id = (@id)", new
                {
                    Id = obj.Id,
                    DataHora = obj.DataHora,
                    IdCliente = obj.IdCliente,
                    Total = obj.Total
                });
            }           
        } 
        /// <summary>       
        ///  Exclui o pedido na base de dados
        /// </summary>
        public void Deletar(Pedido obj)
        {
            using(var conexao = con.CriarConexao())
            {
                conexao.Query($@"DELETE FROM pedido WHERE id = @Id", new
                {
                    Id = obj.Id
                });
            }        
        }
        /// <summary>  
        ///  Obtem o pedido a partir do seu ID na base de dados
        /// </summary>        
        public Pedido Obter(int id)
        {            
           var sql = "SELECT id, datahora, idcliente, total FROM pedido WHERE id = @id";
            using(var conexao = con.CriarConexao())
            {                
                return conexao.QueryFirstOrDefault<Pedido>(sql, new { Id = id });
            }           
        }
        /// <summary>  
        /// Obtem todos os pedido na base de dados
        /// </summary> 
        public IEnumerable<Pedido> ObterTodos()
        {
            var sql = "SELECT id, dataHora, idCliente, total FROM pedido";

            using(var conexao = con.CriarConexao())
            {
                return conexao.Query<Pedido>(sql);
            }
        }     
    }
}