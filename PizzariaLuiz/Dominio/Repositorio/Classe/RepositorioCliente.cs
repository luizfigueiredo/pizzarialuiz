using System.Collections.Generic;
using PizzariaLuiz.Dominio.Entidade;
using PizzariaLuiz.Dominio.Repositorio.Interface;
using Dapper;

namespace PizzariaLuiz.Dominio.Repositorio.Classe
{
    public class RepositorioCliente : IRepositorio<Cliente>
    {
        IConnection con; 
               
        public RepositorioCliente(IConnection conexao)
        {
            this.con = conexao;
        }
        /// <summary>  
        ///  Inseri o cliente na base de dados
        /// </summary> 
        public void Inserir(Cliente obj)
        {
            using(var conexao = con.CriarConexao())
            {
                conexao.Query($@"INSERT INTO cliente VALUES
                                (@Id, @Nome, @Endereco, @Telefone)", new
                {
                    Id = obj.Id,
                    Nome = obj.Nome,
                    Endereco = obj.Endereco,
                    Telefone = obj.Telefone
                });
            }            
        }
        /// <summary>  
        ///  Atualiza o cliente na base de dados
        /// </summary> 
        public void Atualizar(Cliente obj)
        {
            using(var conexao = con.CriarConexao())
            {
                conexao.Query($@"UPDATE cliente SET 
                            nome = @nome, 
                            endereco = @endereco, 
                            telefone = @telefone 
                            where id = (@id)", new
                {
                    Id = obj.Id,
                    Nome = obj.Nome,
                    Endereco = obj.Endereco,
                    Telefone = obj.Telefone
                });
            }           
        } 
        /// <summary>       
        ///  Exclui o cliente na base de dados
        /// </summary> 
        public void Deletar(Cliente obj)
        {
            using(var conexao = con.CriarConexao())
            {
                conexao.Query($@"DELETE FROM cliente WHERE id = @Id", new
                {
                    Id = obj.Id
                });
            }        
        }        
        /// <summary>  
        ///  Obtem o cliente a partir do seu ID na base de dados
        /// </summary> 
        public Cliente Obter(int id)
        {   
            var sql = "SELECT id, nome, endereco, telefone FROM cliente WHERE id = @id";
            using(var conexao = con.CriarConexao())
            {                
                return conexao.QueryFirstOrDefault<Cliente>(sql, new { Id = id });
            }          
        }
        /// <summary>  
        /// Obtem todos os clientes na base de dados
        /// </summary> 
        public IEnumerable<Cliente> ObterTodos()
        {
            var sql = "SELECT id, nome, endereco, telefone FROM cliente";

            using(var conexao = con.CriarConexao())
            {
                return conexao.Query<Cliente>(sql);
            }
        }   
    }
}