using System.Collections.Generic;
using PizzariaLuiz.Dominio.Entidade;
using PizzariaLuiz.Dominio.Repositorio.Interface;
using Dapper;

namespace PizzariaLuiz.Dominio.Repositorio.Classe
{
    public class RepositorioListaPedidos : IRepositorio<ListaPedidos>
    {
      IConnection con;

        public RepositorioListaPedidos(IConnection conexao)
        {
            this.con = conexao;
        }

        public void Atualizar(ListaPedidos obj)
        {
            throw new System.NotImplementedException();
        }

        public void Deletar(ListaPedidos obj)
        {
            throw new System.NotImplementedException();
        }

        public void Inserir(ListaPedidos obj)
        {
            throw new System.NotImplementedException();
        }

        public ListaPedidos Obter(int id)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>  
        /// Obtem todos os pedidos na base de dados
        /// </summary> 
        public IEnumerable<ListaPedidos> ObterTodos()
        {
            var sql = "SELECT * FROM vwpedidos";

            using(var conexao = con.CriarConexao())
            {
                return conexao.Query<ListaPedidos>(sql);
            }
        }         
    }
}